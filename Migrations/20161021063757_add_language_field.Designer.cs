﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Pastebin;

namespace pastebin.Migrations
{
    [DbContext(typeof(PastebinDbContext))]
    [Migration("20161021063757_add_language_field")]
    partial class add_language_field
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431");

            modelBuilder.Entity("Pastebin.Paste", b =>
                {
                    b.Property<string>("VisiblePasteId");

                    b.Property<string>("PasteContents");

                    b.Property<string>("PasteLanguage");

                    b.Property<DateTime>("PasteTime");

                    b.Property<string>("PasteTitle");

                    b.HasKey("VisiblePasteId");

                    b.ToTable("Pastes");
                });
        }
    }
}
