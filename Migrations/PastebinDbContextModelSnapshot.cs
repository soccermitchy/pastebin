﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Pastebin;

namespace pastebin.Migrations
{
    [DbContext(typeof(PastebinDbContext))]
    partial class PastebinDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431");

            modelBuilder.Entity("Pastebin.Paste", b =>
                {
                    b.Property<string>("VisiblePasteId");

                    b.Property<string>("PasteContents");

                    b.Property<string>("PasteLanguage");

                    b.Property<DateTime>("PasteTime");

                    b.Property<string>("PasteTitle");

                    b.HasKey("VisiblePasteId");

                    b.ToTable("Pastes");
                });
        }
    }
}
