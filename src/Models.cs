using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
namespace Pastebin
{
    public class Paste {
        [Key]
        public string VisiblePasteId { get; set; }
        public string PasteTitle { get; set; }
        public string PasteContents { get; set; }
        public DateTime PasteTime { get;set; }
        public string PasteLanguage { get; set; }
    }
    public class PastebinDbContext : DbContext {
        public DbSet<Paste> Pastes { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            optionsBuilder.UseSqlite("Filename=./pastebin.db");
        }
    }
}