using System;
using Nancy;
using Nancy.Conventions;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;
//using Nancy.Session.Persistable;
//using Nancy.Session.InMemory;
namespace Pastebin
{
    public class CustomBootstrapper : DefaultNancyBootstrapper
    {
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);
            //PersistableSessions.Enable(pipelines, new InMemorySessionConfiguration());
        }
        protected override void ConfigureConventions(NancyConventions conventions)
        {
            Console.WriteLine("Custom conventions in use");
            base.ConfigureConventions(conventions);

            conventions.StaticContentsConventions.Add(
                StaticContentConventionBuilder.AddDirectory("/", "/public")
            );

        }
    }
}