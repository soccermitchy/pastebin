using Nancy;

namespace Pastebin {
    public class ApiModule : NancyModule {
        public ApiModule() : base("/api") {
            Get("/p/{id}", args => Api.ApiGetPaste(args.id));
            Get("/p", args => Api.ApiListPastes());
            Post("/p", args => Api.CreatePaste(this));
        }
    }
}