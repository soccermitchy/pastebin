namespace Pastebin {
    public class ApiStatus {
        public bool Status { get; set; }
        public string Message { get; set; }
        public object Result { get; set; }
    }
}