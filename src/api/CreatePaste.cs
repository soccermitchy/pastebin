using System;
using System.Linq;
using Nancy;
using Nancy.ModelBinding;

namespace Pastebin
{
    public static partial class Api {
        public static ApiStatus CreatePaste(NancyModule api) {
            var post = api.Bind<Paste>(f => f.VisiblePasteId,
                                       f => f.PasteTime);
            if (post.PasteContents == null) {
                return new ApiStatus()
                {
                    Status = false,
                    Message = "PasteContents is null"
                };
            }
            
            if (post.PasteTitle == null) {
                return new ApiStatus()
                {
                    Status = false,
                    Message = "PasteTitle is null"
                };
            }

            var pasteData = CreatePaste(post.PasteTitle, post.PasteContents, post.PasteLanguage);
            if (pasteData != null)
            {
                return new ApiStatus()
                {
                    Status = true,
                    Message = "Paste successfully created",
                    Result = new Paste()
                    {
                        VisiblePasteId = pasteData.VisiblePasteId,
                        PasteTime = pasteData.PasteTime,
                        PasteLanguage = pasteData.PasteLanguage,
                    }
                };
            }
            else {
                return new ApiStatus()
                {
                    Status = false,
                    Message = "Paste was not created"
                };
            }
        }
        public static Paste CreatePaste(string title, string contents, string language) {
            using (var db = new PastebinDbContext())
            {
                Console.WriteLine("creating new paste");
                var id = Util.RandomString(8);
                var paste = db.Pastes.Where(p => p.VisiblePasteId == id);
                int tries = 0;
                while (paste.Count() > 0)
                {
                    Console.WriteLine("paste id " + id + "in use");
                    tries += 1;
                    if (tries == 5) return null;

                    id = null;
                    paste = null;

                    id = Util.RandomString(8);
                    paste = db.Pastes.Where(p => p.VisiblePasteId == id);
                }
                var newPaste = new Paste()
                {
                    VisiblePasteId = id,
                    PasteTitle = title,
                    PasteContents = contents,
                    PasteTime = DateTime.Now,
                    PasteLanguage = language
                };
                db.Pastes.Add(newPaste);
                db.SaveChanges();
                return newPaste;
            }
        }
    } 
}