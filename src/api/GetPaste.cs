using System.Linq;

namespace Pastebin {
    public static partial class Api {
        public static ApiStatus ApiGetPaste(string pasteId) {
            var paste = GetPaste(pasteId);
            if (paste == null) {
                return new ApiStatus()
                {
                    Status = false,
                    Message = "Paste not found"
                };
            }
            return new ApiStatus()
            {
                Status = true,
                Message = "Contents of paste " + pasteId,
                Result = paste,
            };
        }
        public static Paste GetPaste(string pasteId) {
            using (var db = new PastebinDbContext()) {
                var results = db.Pastes.Where(x => x.VisiblePasteId == pasteId);
                if (results.Count() == 0) return null;
                return results.First();
            }
        }
    }
}