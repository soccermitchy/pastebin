using System.Collections.Generic;
using System.Linq;

namespace Pastebin {
    public static partial class Api {
        public static ApiStatus ApiListPastes() {
            return new ApiStatus()
            {
                Status = true,
                Message = "Listing pastes",
                Result = ListPastes()
            };
        }
        public static List<Paste> ListPastes() {
            using (var db = new PastebinDbContext())
                return db.Pastes.AsQueryable().ToList();
        }
    } 
}